// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Materials/Material.h"
#include "Cube.h"
#include "SpeedCubePawn.generated.h"

UCLASS()
class SPEEDCUBEHOPE_API ASpeedCubePawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASpeedCubePawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	void MoveLeft();
	void MoveMiddle();
	void MoveMiddleUp();
	void MoveLeftUp();
	void MoveRightDown();
	void MoveRightUp();
	void MoveTopRight();
	void MoveTopLeft();
	void MoveBottomRight();
	void MoveBottomLeft();
	void MoveMiddleLeft();
	void MoveMiddleRight();
	void MoveFrontLeft();
	void MoveFrontRight();
	void MoveBackRight();
	void MoveBackLeft();
	void MoveMiddleFaceLeft();
	void MoveMiddleFaceRight();
	void PitchCubeUp();
	void PitchCubeDown();
	void RollCubeUp();
	void RollCubeDown();
	void YawCubeUp();
	void YawCubeDown();
	void SetupMovement(uint8);
	void MovePart(int16,int16,int16,int16);
	void CallMovement();
	void DoItAll(int16,int16,int16);
	void ScrambleCube();
	bool CompareArray(int32);
	uint8 CheckIsScrambled();
	void CorrectArray();
	//Cube parts of speed cube
	class UCube* YellowMiddle;
	class UCube* RedMiddle;
	class UCube* GreenMiddle;
	class UCube* BlueMiddle;
	class UCube* OrangeMiddle;
	class UCube* WhiteMiddle;
	class UCube* RYEdge;
	class UCube* RWEdge;
	class UCube* BYEdge;
	class UCube* BWEdge;
	class UCube* GYEdge;
	class UCube* GWEdge;
	class UCube* OYEdge;
	class UCube* OWEdge;
	class UCube* RGEdge;
	class UCube* RBEdge;
	class UCube* OGEdge;
	class UCube* OBEdge;
	class UCube* RYBCorner;
	class UCube* RYGCorner;
	class UCube* RWBCorner;
	class UCube* RWGCorner;
	class UCube* OYBCorner;
	class UCube* OYGCorner;
	class UCube* OWBCorner;
	class UCube* OWGCorner;
	class UCube* GreenSticker;
	class UCube* GreenSticker2;
	class UCube* GreenStickerR;
	class UCube* GreenStickerU;
	class UCube* GreenStickerD;
	class UCube* GreenStickerUR;
	class UCube* GreenStickerDR;
	class UCube* GreenStickerDL;
	class UCube* GreenStickerUL;
	class UCube* OrangeStickerM;
	class UCube* OrangeStickerL;
	class UCube* OrangeStickerR;
	class UCube* OrangeStickerU;
	class UCube* OrangeStickerD;
	class UCube* OrangeStickerUR;
	class UCube* OrangeStickerUL;
	class UCube* OrangeStickerDR;
	class UCube* OrangeStickerDL;
	class UCube* RedStickerM;
	class UCube* RedStickerL;
	class UCube* RedStickerR;
	class UCube* RedStickerU;
	class UCube* RedStickerD;
	class UCube* RedStickerUR;
	class UCube* RedStickerUL;
	class UCube* RedStickerDR;
	class UCube* RedStickerDL;
	class UCube* BlueStickerM;
	class UCube* BlueStickerL;
	class UCube* BlueStickerR;
	class UCube* BlueStickerU;
	class UCube* BlueStickerD;
	class UCube* BlueStickerUR;
	class UCube* BlueStickerUL;
	class UCube* BlueStickerDR;
	class UCube* BlueStickerDL;
	class UCube* WhiteStickerM;
	class UCube* WhiteStickerL;
	class UCube* WhiteStickerR;
	class UCube* WhiteStickerU;
	class UCube* WhiteStickerD;
	class UCube* WhiteStickerUR;
	class UCube* WhiteStickerUL;
	class UCube* WhiteStickerDR;
	class UCube* WhiteStickerDL;
	class UCube* YellowStickerM;
	class UCube* YellowStickerL;
	class UCube* YellowStickerR;
	class UCube* YellowStickerU;
	class UCube* YellowStickerD;
	class UCube* YellowStickerUR;
	class UCube* YellowStickerUL;
	class UCube* YellowStickerDR;
	class UCube* YellowStickerDL;
	//UPROPERTY(EditAnywhere)
	class UMaterial* StoredMaterial;
	class UMaterialInstanceDynamic* GreenMatInst;
	class UMaterialInstanceDynamic* OrangeMatInst;
	class UMaterialInstanceDynamic* RedMatInst;
	class UMaterialInstanceDynamic* BlueMatInst;
	class UMaterialInstanceDynamic* WhiteMatInst;
	class UMaterialInstanceDynamic* YellowMatInst;
	bool bInputTaken;
	int32 SideChange;
	int32 Times;
	int32 Edges[12] = {};
	//Handle to manage the timer
	FTimerHandle RepeatTimerHandle;
};
