// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SpeedCubeHopeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SPEEDCUBEHOPE_API ASpeedCubeHopeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
