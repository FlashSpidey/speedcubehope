// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedCubePawn.h"
#include "Components/BoxComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/MeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Cube.h"

// Sets default values
ASpeedCubePawn::ASpeedCubePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	UBoxComponent* Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	RootComponent = Box;
	SideChange = 0;
	//UMaterialInstanceDynamic* GreenMat = UMaterialInstanceDynamic::Create(GreenMat, GreenMiddle);
	//GreenMat->SetVectorParameterValue(TEXT("VectorProperty"), FLinearColor::Green);
	//make the small cubes of the whole cube and set static mesh
	YellowMiddle = CreateDefaultSubobject<UCube>(TEXT("CubeVisual"));
	YellowMiddle->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/Blender/Tests/Chamfed.Chamfed"));
	//YellowMat = CreateDefaultSubobject<UMaterial>(TEXT("Yellow Color"));
	if(CubeVisualAsset.Succeeded())
	{
		YellowMiddle->SetStaticMesh(CubeVisualAsset.Object);
		YellowMiddle->FullSet(FVector(0.0f,0.0f,200.0f),200);
		//YellowMiddle->SetMaterial(0,YellowMat);
	}
	RedMiddle = CreateDefaultSubobject<UCube>(TEXT("Middle of Red"));
	RedMiddle->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		RedMiddle->SetStaticMesh(CubeVisualAsset.Object);
		RedMiddle->FullSet(FVector(0.0f,200.0f,0.0f),200);
	}
	//GreenMat = CreateDefaultSubobject<UMaterial>(TEXT("Yellow Color"));
	GreenMiddle = CreateDefaultSubobject<UCube>(TEXT("Middle of green"));
	GreenMiddle->SetupAttachment(RootComponent);
	//static ConstructorHelpers::FObjectFinder<UStaticMesh> GMVisualAsset(TEXT("/Game/Blender/Deletion/CubePieces_Cube_066.CubePieces_Cube_066"));
	if(CubeVisualAsset.Succeeded())
	{
		GreenMiddle->SetStaticMesh(CubeVisualAsset.Object);
		GreenMiddle->FullSet(FVector(-200.0f,0.0f,0.0f),200);
		//GreenMiddle->SetMaterial(0,GreenMat);
	}
	BlueMiddle = CreateDefaultSubobject<UCube>(TEXT("Middle of blue"));
	BlueMiddle->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		BlueMiddle->SetStaticMesh(CubeVisualAsset.Object);
		BlueMiddle->FullSet(FVector(200.0f,0.0f,0.0f),200);
	}
	OrangeMiddle = CreateDefaultSubobject<UCube>(TEXT("Middle of orange"));
	OrangeMiddle->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		OrangeMiddle->SetStaticMesh(CubeVisualAsset.Object);
		OrangeMiddle->FullSet(FVector(0.0f,-200.0f,0.0f),200);
	}
	WhiteMiddle = CreateDefaultSubobject<UCube>(TEXT("Middle of White"));
	WhiteMiddle->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		WhiteMiddle->SetStaticMesh(CubeVisualAsset.Object);
		WhiteMiddle->FullSet(FVector(0.0f,0.0f,-200.0f),200);
	}
	RYEdge = CreateDefaultSubobject<UCube>(TEXT("edge w/ red and yellow"));
	RYEdge->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		RYEdge->SetStaticMesh(CubeVisualAsset.Object);
		RYEdge->FullSet(FVector(0.0f,200.0f,200.0f),283);
	}
	RWEdge = CreateDefaultSubobject<UCube>(TEXT("edge w/ red and white"));
	RWEdge->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		RWEdge->SetStaticMesh(CubeVisualAsset.Object);
		RWEdge->FullSet(FVector(0.0f,200.0f,-200.0f),283);
	}
	BYEdge = CreateDefaultSubobject<UCube>(TEXT("edge w/ blue and yellow"));
	BYEdge->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		BYEdge->SetStaticMesh(CubeVisualAsset.Object);
		BYEdge->FullSet(FVector(200.0f,0.0f,200.0f),283);
	}
	BWEdge = CreateDefaultSubobject<UCube>(TEXT("edge w/ blue and white"));
	BWEdge->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		BWEdge->SetStaticMesh(CubeVisualAsset.Object);
		BWEdge->FullSet(FVector(200.0f,0.0f,-200.0f),283);
	}
	GWEdge = CreateDefaultSubobject<UCube>(TEXT("edge w/ green and white"));
	GWEdge->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		GWEdge->SetStaticMesh(CubeVisualAsset.Object);
		GWEdge->FullSet(FVector(-200.0f,0.0f,-200.0f), 283);
	}
	GYEdge = CreateDefaultSubobject<UCube>(TEXT("edge w/ green and yellow"));
	GYEdge->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		GYEdge->SetStaticMesh(CubeVisualAsset.Object);
		GYEdge->FullSet(FVector(-200.0f,0.0f,200.0f),283);
	}
	OYEdge = CreateDefaultSubobject<UCube>(TEXT("edge w/ orange and yellow"));
	OYEdge->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		OYEdge->SetStaticMesh(CubeVisualAsset.Object);
		OYEdge->FullSet(FVector(0.0f,-200.0f,200.0f),283);
	}
	OWEdge = CreateDefaultSubobject<UCube>(TEXT("edge w/ orange and white"));
	OWEdge->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		OWEdge->SetStaticMesh(CubeVisualAsset.Object);
		OWEdge->FullSet(FVector(0.0f,-200.0f,-200.0f),283);
	}
	RGEdge = CreateDefaultSubobject<UCube>(TEXT("edge w/ red and green"));
	RGEdge->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		RGEdge->SetStaticMesh(CubeVisualAsset.Object);
		RGEdge->FullSet(FVector(-200.0f,200.0f,0.0f),283);
	}
	RBEdge = CreateDefaultSubobject<UCube>(TEXT("edge w/ red and blue"));
	RBEdge->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		RBEdge->SetStaticMesh(CubeVisualAsset.Object);
		RBEdge->FullSet(FVector(200.0f,200.0f,0.0f),283);
	}
	OGEdge = CreateDefaultSubobject<UCube>(TEXT("edge w/ orange and green"));
	OGEdge->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		OGEdge->SetStaticMesh(CubeVisualAsset.Object);
		OGEdge->FullSet(FVector(-200.0f,-200.0f,0.0f),283);
	}
	OBEdge = CreateDefaultSubobject<UCube>(TEXT("edge w/ orange and blue"));
	OBEdge->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		OBEdge->SetStaticMesh(CubeVisualAsset.Object);
		OBEdge->FullSet(FVector(200.0f,-200.0f,0.0f),283);
	}
	RYBCorner = CreateDefaultSubobject<UCube>(TEXT("Corner w/ red, yellow, blue"));
	RYBCorner->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		RYBCorner->SetStaticMesh(CubeVisualAsset.Object);
		RYBCorner->FullSet(FVector(200.0f,200.0f,200.0f),347);
	}
	RYGCorner = CreateDefaultSubobject<UCube>(TEXT("Corner w/ red, yellow, green"));
	RYGCorner->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		RYGCorner->SetStaticMesh(CubeVisualAsset.Object);
		RYGCorner->FullSet(FVector(-200.0f,200.0f,200.0f),347);
	}
	RWBCorner = CreateDefaultSubobject<UCube>(TEXT("Corner w/ red, white, blue"));
	RWBCorner->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		RWBCorner->SetStaticMesh(CubeVisualAsset.Object);
		RWBCorner->FullSet(FVector(200.0f,200.0f,-200.0f),347);
	}
	RWGCorner = CreateDefaultSubobject<UCube>(TEXT("Corner w/ red, white, green"));
	RWGCorner->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		RWGCorner->SetStaticMesh(CubeVisualAsset.Object);
		RWGCorner->FullSet(FVector(-200.0f,200.0f,-200.0f),347);
	}
	OYBCorner = CreateDefaultSubobject<UCube>(TEXT("Corner w/ orange, yellow, blue"));
	OYBCorner->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		OYBCorner->SetStaticMesh(CubeVisualAsset.Object);
		OYBCorner->FullSet(FVector(200.0f,-200.0f,200.0f),347);
	}
	OYGCorner = CreateDefaultSubobject<UCube>(TEXT("Corner w/ orange, yellow, green"));
	OYGCorner->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		OYGCorner->SetStaticMesh(CubeVisualAsset.Object);
		OYGCorner->FullSet(FVector(-200.0f,-200.0f,200.0f),347);
	}
	OWBCorner = CreateDefaultSubobject<UCube>(TEXT("Corner w/ orange, white, blue"));
	OWBCorner->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		OWBCorner->SetStaticMesh(CubeVisualAsset.Object);
		OWBCorner->FullSet(FVector(200.0f,-200.0f,-200.0f),347);
	}
	OWGCorner = CreateDefaultSubobject<UCube>(TEXT("Corner w/ orange, white, green"));
	OWGCorner->SetupAttachment(RootComponent);
	if(CubeVisualAsset.Succeeded())
	{
		OWGCorner->SetStaticMesh(CubeVisualAsset.Object);
		OWGCorner->FullSet(FVector(-200.0f,-200.0f,-200.0f),347);
	}
	static ConstructorHelpers::FObjectFinder<UStaticMesh> StickerAsset(TEXT("/Game/Blender/Stickers/Sticker.Sticker"));
	GreenSticker = CreateDefaultSubobject<UCube>(TEXT("Sticker of green"));
	GreenSticker->SetupAttachment(GreenMiddle);
	if(StickerAsset.Succeeded())
	{
		GreenSticker->SetStaticMesh(StickerAsset.Object);
		GreenSticker->FullSet2(FVector(-50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	GreenSticker2 = CreateDefaultSubobject<UCube>(TEXT("Sticker of green number 2"));
	GreenSticker2->SetupAttachment(OGEdge);
	if(StickerAsset.Succeeded())
	{
		GreenSticker2->SetStaticMesh(StickerAsset.Object);
		GreenSticker2->FullSet2(FVector(-50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	GreenStickerR = CreateDefaultSubobject<UCube>(TEXT("Sticker of green 3"));
	GreenStickerR->SetupAttachment(RGEdge);
	if(StickerAsset.Succeeded())
	{
		GreenStickerR->SetStaticMesh(StickerAsset.Object);
		GreenStickerR->FullSet2(FVector(-50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	GreenStickerU = CreateDefaultSubobject<UCube>(TEXT("Sticker of green 4"));
	GreenStickerU->SetupAttachment(GYEdge);
	if(StickerAsset.Succeeded())
	{
		GreenStickerU->SetStaticMesh(StickerAsset.Object);
		GreenStickerU->FullSet2(FVector(-50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	GreenStickerD = CreateDefaultSubobject<UCube>(TEXT("Sticker of green 5"));
	GreenStickerD->SetupAttachment(GWEdge);
	if(StickerAsset.Succeeded())
	{
		GreenStickerD->SetStaticMesh(StickerAsset.Object);
		GreenStickerD->FullSet2(FVector(-50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	GreenStickerUR = CreateDefaultSubobject<UCube>(TEXT("Sticker of green top right"));
	GreenStickerUR->SetupAttachment(RYGCorner);
	if(StickerAsset.Succeeded())
	{
		GreenStickerUR->SetStaticMesh(StickerAsset.Object);
		GreenStickerUR->FullSet2(FVector(-50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	GreenStickerUL = CreateDefaultSubobject<UCube>(TEXT("Sticker of green top left"));
	GreenStickerUL->SetupAttachment(OYGCorner);
	if(StickerAsset.Succeeded())
	{
		GreenStickerUL->SetStaticMesh(StickerAsset.Object);
		GreenStickerUL->FullSet2(FVector(-50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	GreenStickerDL = CreateDefaultSubobject<UCube>(TEXT("Sticker of green bottom left"));
	GreenStickerDL->SetupAttachment(OWGCorner);
	if(StickerAsset.Succeeded())
	{
		GreenStickerDL->SetStaticMesh(StickerAsset.Object);
		GreenStickerDL->FullSet2(FVector(-50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	GreenStickerDR = CreateDefaultSubobject<UCube>(TEXT("Sticker of green bottom right"));
	GreenStickerDR->SetupAttachment(RWGCorner);
	if(StickerAsset.Succeeded())
	{
		GreenStickerDR->SetStaticMesh(StickerAsset.Object);
		GreenStickerDR->FullSet2(FVector(-50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	OrangeStickerM = CreateDefaultSubobject<UCube>(TEXT("Sticker of Orange in middle"));
	OrangeStickerM->SetupAttachment(OrangeMiddle);
	if(StickerAsset.Succeeded())
	{
		OrangeStickerM->SetStaticMesh(StickerAsset.Object);
		OrangeStickerM->FullSet2(FVector(0.0f,-50.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	OrangeStickerL = CreateDefaultSubobject<UCube>(TEXT("Sticker of Orange left"));
	OrangeStickerL->SetupAttachment(OBEdge);
	if(StickerAsset.Succeeded())
	{
		OrangeStickerL->SetStaticMesh(StickerAsset.Object);
		OrangeStickerL->FullSet2(FVector(0.0f,-50.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	OrangeStickerR = CreateDefaultSubobject<UCube>(TEXT("Sticker of Orange right"));
	OrangeStickerR->SetupAttachment(OGEdge);
	if(StickerAsset.Succeeded())
	{
		OrangeStickerR->SetStaticMesh(StickerAsset.Object);
		OrangeStickerR->FullSet2(FVector(0.0f,-50.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	OrangeStickerU = CreateDefaultSubobject<UCube>(TEXT("Sticker of Orange above"));
	OrangeStickerU->SetupAttachment(OYEdge);
	if(StickerAsset.Succeeded())
	{
		OrangeStickerU->SetStaticMesh(StickerAsset.Object);
		OrangeStickerU->FullSet2(FVector(0.0f,-50.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	OrangeStickerD = CreateDefaultSubobject<UCube>(TEXT("Sticker of Orange below"));
	OrangeStickerD->SetupAttachment(OWEdge);
	if(StickerAsset.Succeeded())
	{
		OrangeStickerD->SetStaticMesh(StickerAsset.Object);
		OrangeStickerD->FullSet2(FVector(0.0f,-50.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	OrangeStickerUR = CreateDefaultSubobject<UCube>(TEXT("Sticker of Orange top right"));
	OrangeStickerUR->SetupAttachment(OYGCorner);
	if(StickerAsset.Succeeded())
	{
		OrangeStickerUR->SetStaticMesh(StickerAsset.Object);
		OrangeStickerUR->FullSet2(FVector(0.0f,-50.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	OrangeStickerUL = CreateDefaultSubobject<UCube>(TEXT("Sticker of Orange top left"));
	OrangeStickerUL->SetupAttachment(OYBCorner);
	if(StickerAsset.Succeeded())
	{
		OrangeStickerUL->SetStaticMesh(StickerAsset.Object);
		OrangeStickerUL->FullSet2(FVector(0.0f,-50.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	OrangeStickerDL = CreateDefaultSubobject<UCube>(TEXT("Sticker of Orange bottom left"));
	OrangeStickerDL->SetupAttachment(OWBCorner);
	if(StickerAsset.Succeeded())
	{
		OrangeStickerDL->SetStaticMesh(StickerAsset.Object);
		OrangeStickerDL->FullSet2(FVector(0.0f,-50.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	OrangeStickerDR = CreateDefaultSubobject<UCube>(TEXT("Sticker of Orange bottom right"));
	OrangeStickerDR->SetupAttachment(OWGCorner);
	if(StickerAsset.Succeeded())
	{
		OrangeStickerDR->SetStaticMesh(StickerAsset.Object);
		OrangeStickerDR->FullSet2(FVector(0.0f,-50.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	RedStickerM = CreateDefaultSubobject<UCube>(TEXT("Sticker of Red in middle"));
	RedStickerM->SetupAttachment(RedMiddle);
	if(StickerAsset.Succeeded())
	{
		RedStickerM->SetStaticMesh(StickerAsset.Object);
		RedStickerM->FullSet2(FVector(0.0f,51.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	RedStickerL = CreateDefaultSubobject<UCube>(TEXT("Sticker of Red left"));
	RedStickerL->SetupAttachment(RGEdge);
	if(StickerAsset.Succeeded())
	{
		RedStickerL->SetStaticMesh(StickerAsset.Object);
		RedStickerL->FullSet2(FVector(0.0f,51.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	RedStickerR = CreateDefaultSubobject<UCube>(TEXT("Sticker of Red right"));
	RedStickerR->SetupAttachment(RBEdge);
	if(StickerAsset.Succeeded())
	{
		RedStickerR->SetStaticMesh(StickerAsset.Object);
		RedStickerR->FullSet2(FVector(0.0f,51.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	RedStickerU = CreateDefaultSubobject<UCube>(TEXT("Sticker of Red above"));
	RedStickerU->SetupAttachment(RYEdge);
	if(StickerAsset.Succeeded())
	{
		RedStickerU->SetStaticMesh(StickerAsset.Object);
		RedStickerU->FullSet2(FVector(0.0f,51.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	RedStickerD = CreateDefaultSubobject<UCube>(TEXT("Sticker of Red below"));
	RedStickerD->SetupAttachment(RWEdge);
	if(StickerAsset.Succeeded())
	{
		RedStickerD->SetStaticMesh(StickerAsset.Object);
		RedStickerD->FullSet2(FVector(0.0f,51.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	RedStickerUR = CreateDefaultSubobject<UCube>(TEXT("Sticker of Red top right"));
	RedStickerUR->SetupAttachment(RYBCorner);
	if(StickerAsset.Succeeded())
	{
		RedStickerUR->SetStaticMesh(StickerAsset.Object);
		RedStickerUR->FullSet2(FVector(0.0f,51.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	RedStickerUL = CreateDefaultSubobject<UCube>(TEXT("Sticker of Red top left"));
	RedStickerUL->SetupAttachment(RYGCorner);
	if(StickerAsset.Succeeded())
	{
		RedStickerUL->SetStaticMesh(StickerAsset.Object);
		RedStickerUL->FullSet2(FVector(0.0f,51.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	RedStickerDL = CreateDefaultSubobject<UCube>(TEXT("Sticker of Red bottom left"));
	RedStickerDL->SetupAttachment(RWGCorner);
	if(StickerAsset.Succeeded())
	{
		RedStickerDL->SetStaticMesh(StickerAsset.Object);
		RedStickerDL->FullSet2(FVector(0.0f,51.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	RedStickerDR = CreateDefaultSubobject<UCube>(TEXT("Sticker of Red bottom right"));
	RedStickerDR->SetupAttachment(RWBCorner);
	if(StickerAsset.Succeeded())
	{
		RedStickerDR->SetStaticMesh(StickerAsset.Object);
		RedStickerDR->FullSet2(FVector(0.0f,51.0f,0.0f),FRotator(90.0f,90.0f,0.0f),201);
	}
	BlueStickerM = CreateDefaultSubobject<UCube>(TEXT("Sticker of Blue in middle"));
	BlueStickerM->SetupAttachment(BlueMiddle);
	if(StickerAsset.Succeeded())
	{
		BlueStickerM->SetStaticMesh(StickerAsset.Object);
		BlueStickerM->FullSet2(FVector(50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	BlueStickerL = CreateDefaultSubobject<UCube>(TEXT("Sticker of Blue left"));
	BlueStickerL->SetupAttachment(RBEdge);
	if(StickerAsset.Succeeded())
	{
		BlueStickerL->SetStaticMesh(StickerAsset.Object);
		BlueStickerL->FullSet2(FVector(50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	BlueStickerR = CreateDefaultSubobject<UCube>(TEXT("Sticker of Blue right"));
	BlueStickerR->SetupAttachment(OBEdge);
	if(StickerAsset.Succeeded())
	{
		BlueStickerR->SetStaticMesh(StickerAsset.Object);
		BlueStickerR->FullSet2(FVector(50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	BlueStickerU = CreateDefaultSubobject<UCube>(TEXT("Sticker of Blue above"));
	BlueStickerU->SetupAttachment(BYEdge);
	if(StickerAsset.Succeeded())
	{
		BlueStickerU->SetStaticMesh(StickerAsset.Object);
		BlueStickerU->FullSet2(FVector(50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	BlueStickerD = CreateDefaultSubobject<UCube>(TEXT("Sticker of Blue below"));
	BlueStickerD->SetupAttachment(BWEdge);
	if(StickerAsset.Succeeded())
	{
		BlueStickerD->SetStaticMesh(StickerAsset.Object);
		BlueStickerD->FullSet2(FVector(50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	BlueStickerUR = CreateDefaultSubobject<UCube>(TEXT("Sticker of Blue top right"));
	BlueStickerUR->SetupAttachment(OYBCorner);
	if(StickerAsset.Succeeded())
	{
		BlueStickerUR->SetStaticMesh(StickerAsset.Object);
		BlueStickerUR->FullSet2(FVector(50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	BlueStickerUL = CreateDefaultSubobject<UCube>(TEXT("Sticker of Blue top left"));
	BlueStickerUL->SetupAttachment(RYBCorner);
	if(StickerAsset.Succeeded())
	{
		BlueStickerUL->SetStaticMesh(StickerAsset.Object);
		BlueStickerUL->FullSet2(FVector(50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	BlueStickerDL = CreateDefaultSubobject<UCube>(TEXT("Sticker of Blue bottom left"));
	BlueStickerDL->SetupAttachment(RWBCorner);
	if(StickerAsset.Succeeded())
	{
		BlueStickerDL->SetStaticMesh(StickerAsset.Object);
		BlueStickerDL->FullSet2(FVector(50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	BlueStickerDR = CreateDefaultSubobject<UCube>(TEXT("Sticker of Blue bottom right"));
	BlueStickerDR->SetupAttachment(OWBCorner);
	if(StickerAsset.Succeeded())
	{
		BlueStickerDR->SetStaticMesh(StickerAsset.Object);
		BlueStickerDR->FullSet2(FVector(50.0f,0.0f,0.0f),FRotator(90.0f,0.0f,0.0f),201);
	}
	YellowStickerM = CreateDefaultSubobject<UCube>(TEXT("Sticker of Yellow in middle"));
	YellowStickerM->SetupAttachment(YellowMiddle);
	if(StickerAsset.Succeeded())
	{
		YellowStickerM->SetStaticMesh(StickerAsset.Object);
		YellowStickerM->FullSet2(FVector(0.0f,0.0f,50.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	YellowStickerL = CreateDefaultSubobject<UCube>(TEXT("Sticker of Yellow left"));
	YellowStickerL->SetupAttachment(OYEdge);
	if(StickerAsset.Succeeded())
	{
		YellowStickerL->SetStaticMesh(StickerAsset.Object);
		YellowStickerL->FullSet2(FVector(0.0f,0.0f,50.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	YellowStickerR = CreateDefaultSubobject<UCube>(TEXT("Sticker of Yellow right"));
	YellowStickerR->SetupAttachment(RYEdge);
	if(StickerAsset.Succeeded())
	{
		YellowStickerR->SetStaticMesh(StickerAsset.Object);
		YellowStickerR->FullSet2(FVector(0.0f,0.0f,50.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	YellowStickerU = CreateDefaultSubobject<UCube>(TEXT("Sticker of Yellow above"));
	YellowStickerU->SetupAttachment(BYEdge);
	if(StickerAsset.Succeeded())
	{
		YellowStickerU->SetStaticMesh(StickerAsset.Object);
		YellowStickerU->FullSet2(FVector(0.0f,0.0f,50.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	YellowStickerD = CreateDefaultSubobject<UCube>(TEXT("Sticker of Yellow below"));
	YellowStickerD->SetupAttachment(GYEdge);
	if(StickerAsset.Succeeded())
	{
		YellowStickerD->SetStaticMesh(StickerAsset.Object);
		YellowStickerD->FullSet2(FVector(0.0f,0.0f,50.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	YellowStickerUR = CreateDefaultSubobject<UCube>(TEXT("Sticker of Yellow top right"));
	YellowStickerUR->SetupAttachment(RYBCorner);
	if(StickerAsset.Succeeded())
	{
		YellowStickerUR->SetStaticMesh(StickerAsset.Object);
		YellowStickerUR->FullSet2(FVector(0.0f,0.0f,50.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	YellowStickerUL = CreateDefaultSubobject<UCube>(TEXT("Sticker of Yellow top left"));
	YellowStickerUL->SetupAttachment(OYBCorner);
	if(StickerAsset.Succeeded())
	{
		YellowStickerUL->SetStaticMesh(StickerAsset.Object);
		YellowStickerUL->FullSet2(FVector(0.0f,0.0f,50.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	YellowStickerDL = CreateDefaultSubobject<UCube>(TEXT("Sticker of Yellow bottom left"));
	YellowStickerDL->SetupAttachment(OYGCorner);
	if(StickerAsset.Succeeded())
	{
		YellowStickerDL->SetStaticMesh(StickerAsset.Object);
		YellowStickerDL->FullSet2(FVector(0.0f,0.0f,50.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	YellowStickerDR = CreateDefaultSubobject<UCube>(TEXT("Sticker of Yellow bottom right"));
	YellowStickerDR->SetupAttachment(RYGCorner);
	if(StickerAsset.Succeeded())
	{
		YellowStickerDR->SetStaticMesh(StickerAsset.Object);
		YellowStickerDR->FullSet2(FVector(0.0f,0.0f,50.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	WhiteStickerM = CreateDefaultSubobject<UCube>(TEXT("Sticker of White in middle"));
	WhiteStickerM->SetupAttachment(WhiteMiddle);
	if(StickerAsset.Succeeded())
	{
		WhiteStickerM->SetStaticMesh(StickerAsset.Object);
		WhiteStickerM->FullSet2(FVector(0.0f,0.0f,-51.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	WhiteStickerL = CreateDefaultSubobject<UCube>(TEXT("Sticker of White left"));
	WhiteStickerL->SetupAttachment(OWEdge);
	if(StickerAsset.Succeeded())
	{
		WhiteStickerL->SetStaticMesh(StickerAsset.Object);
		WhiteStickerL->FullSet2(FVector(0.0f,0.0f,-51.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	WhiteStickerR = CreateDefaultSubobject<UCube>(TEXT("Sticker of White right"));
	WhiteStickerR->SetupAttachment(RWEdge);
	if(StickerAsset.Succeeded())
	{
		WhiteStickerR->SetStaticMesh(StickerAsset.Object);
		WhiteStickerR->FullSet2(FVector(0.0f,0.0f,-51.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	WhiteStickerU = CreateDefaultSubobject<UCube>(TEXT("Sticker of White above"));
	WhiteStickerU->SetupAttachment(GWEdge);
	if(StickerAsset.Succeeded())
	{
		WhiteStickerU->SetStaticMesh(StickerAsset.Object);
		WhiteStickerU->FullSet2(FVector(0.0f,0.0f,-51.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	WhiteStickerD = CreateDefaultSubobject<UCube>(TEXT("Sticker of White below"));
	WhiteStickerD->SetupAttachment(BWEdge);
	if(StickerAsset.Succeeded())
	{
		WhiteStickerD->SetStaticMesh(StickerAsset.Object);
		WhiteStickerD->FullSet2(FVector(0.0f,0.0f,-51.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	WhiteStickerUR = CreateDefaultSubobject<UCube>(TEXT("Sticker of White top right"));
	WhiteStickerUR->SetupAttachment(RWGCorner);
	if(StickerAsset.Succeeded())
	{
		WhiteStickerUR->SetStaticMesh(StickerAsset.Object);
		WhiteStickerUR->FullSet2(FVector(0.0f,0.0f,-51.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	WhiteStickerUL = CreateDefaultSubobject<UCube>(TEXT("Sticker of White top left"));
	WhiteStickerUL->SetupAttachment(OWGCorner);
	if(StickerAsset.Succeeded())
	{
		WhiteStickerUL->SetStaticMesh(StickerAsset.Object);
		WhiteStickerUL->FullSet2(FVector(0.0f,0.0f,-51.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	WhiteStickerDL = CreateDefaultSubobject<UCube>(TEXT("Sticker of White bottom left"));
	WhiteStickerDL->SetupAttachment(OWBCorner);
	if(StickerAsset.Succeeded())
	{
		WhiteStickerDL->SetStaticMesh(StickerAsset.Object);
		WhiteStickerDL->FullSet2(FVector(0.0f,0.0f,-51.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	WhiteStickerDR = CreateDefaultSubobject<UCube>(TEXT("Sticker of White bottom right"));
	WhiteStickerDR->SetupAttachment(RWBCorner);
	if(StickerAsset.Succeeded())
	{
		WhiteStickerDR->SetStaticMesh(StickerAsset.Object);
		WhiteStickerDR->FullSet2(FVector(0.0f,0.0f,-51.0f),FRotator(0.0f,0.0f,0.0f),201);
	}
	static ConstructorHelpers::FObjectFinder<UMaterial> GreenMaterial(TEXT("/Game/Blender/Deletion/green_000.green_000"));
	if(GreenMaterial.Succeeded())
	{
	    StoredMaterial = GreenMaterial.Object;
    }
    GreenMatInst = UMaterialInstanceDynamic::Create(StoredMaterial, GreenMiddle);
	GreenSticker->SetMaterial(0,GreenMatInst);
	GreenSticker2->SetMaterial(0,GreenMatInst);
	GreenStickerR->SetMaterial(0,GreenMatInst);
	GreenStickerU->SetMaterial(0,GreenMatInst);
	GreenStickerD->SetMaterial(0,GreenMatInst);
	GreenStickerUL->SetMaterial(0,GreenMatInst);
	GreenStickerUR->SetMaterial(0,GreenMatInst);
	GreenStickerDL->SetMaterial(0,GreenMatInst);
	GreenStickerDR->SetMaterial(0,GreenMatInst);
	static ConstructorHelpers::FObjectFinder<UMaterial> OrangeMaterial(TEXT("/Game/Blender/Deletion/Orange_001.Orange_001"));
	if(OrangeMaterial.Succeeded())
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Good"));
	    StoredMaterial = OrangeMaterial.Object;
    }
    OrangeMatInst = UMaterialInstanceDynamic::Create(StoredMaterial, OrangeMiddle);
	OrangeStickerM->SetMaterial(0,OrangeMatInst);
	OrangeStickerL->SetMaterial(0,OrangeMatInst);
	OrangeStickerR->SetMaterial(0,OrangeMatInst);
	OrangeStickerU->SetMaterial(0,OrangeMatInst);
	OrangeStickerD->SetMaterial(0,OrangeMatInst);
	OrangeStickerUL->SetMaterial(0,OrangeMatInst);
	OrangeStickerUR->SetMaterial(0,OrangeMatInst);
	OrangeStickerDL->SetMaterial(0,OrangeMatInst);
	OrangeStickerDR->SetMaterial(0,OrangeMatInst);
	static ConstructorHelpers::FObjectFinder<UMaterial> RedMaterial(TEXT("/Game/Blender/Deletion/red_000.red_000"));
	if(RedMaterial.Succeeded())
	{
	    StoredMaterial = RedMaterial.Object;
    }
	RedMatInst = UMaterialInstanceDynamic::Create(StoredMaterial, RedMiddle);
	RedStickerM->SetMaterial(0,RedMatInst);
	RedStickerL->SetMaterial(0,RedMatInst);
	RedStickerR->SetMaterial(0,RedMatInst);
	RedStickerU->SetMaterial(0,RedMatInst);
	RedStickerD->SetMaterial(0,RedMatInst);
	RedStickerUL->SetMaterial(0,RedMatInst);
	RedStickerUR->SetMaterial(0,RedMatInst);
	RedStickerDL->SetMaterial(0,RedMatInst);
	RedStickerDR->SetMaterial(0,RedMatInst);
	static ConstructorHelpers::FObjectFinder<UMaterial> BlueMaterial(TEXT("/Game/Blender/Deletion/blue_000.blue_000"));
	if(BlueMaterial.Succeeded())
	{
	    StoredMaterial = BlueMaterial.Object;
    }
	BlueMatInst = UMaterialInstanceDynamic::Create(StoredMaterial, BlueMiddle);
	BlueStickerM->SetMaterial(0,BlueMatInst);
	BlueStickerL->SetMaterial(0,BlueMatInst);
	BlueStickerR->SetMaterial(0,BlueMatInst);
	BlueStickerU->SetMaterial(0,BlueMatInst);
	BlueStickerD->SetMaterial(0,BlueMatInst);
	BlueStickerUL->SetMaterial(0,BlueMatInst);
	BlueStickerUR->SetMaterial(0,BlueMatInst);
	BlueStickerDL->SetMaterial(0,BlueMatInst);
	BlueStickerDR->SetMaterial(0,BlueMatInst);
	static ConstructorHelpers::FObjectFinder<UMaterial> YellowMaterial(TEXT("/Game/Blender/Deletion/yellow_000.yellow_000"));
	if(YellowMaterial.Succeeded())
	{
	    StoredMaterial = YellowMaterial.Object;
    }
	YellowMatInst = UMaterialInstanceDynamic::Create(StoredMaterial, YellowMiddle);
	YellowStickerM->SetMaterial(0,YellowMatInst);
	YellowStickerL->SetMaterial(0,YellowMatInst);
	YellowStickerR->SetMaterial(0,YellowMatInst);
	YellowStickerU->SetMaterial(0,YellowMatInst);
	YellowStickerD->SetMaterial(0,YellowMatInst);
	YellowStickerUL->SetMaterial(0,YellowMatInst);
	YellowStickerUR->SetMaterial(0,YellowMatInst);
	YellowStickerDL->SetMaterial(0,YellowMatInst);
	YellowStickerDR->SetMaterial(0,YellowMatInst);
	static ConstructorHelpers::FObjectFinder<UMaterial> WhiteMaterial(TEXT("/Game/Blender/Deletion/white_001.white_001"));
	if(WhiteMaterial.Succeeded())
	{
	    StoredMaterial = WhiteMaterial.Object;
    }
	WhiteMatInst = UMaterialInstanceDynamic::Create(StoredMaterial, WhiteMiddle);
	WhiteStickerM->SetMaterial(0,WhiteMatInst);
	WhiteStickerL->SetMaterial(0,WhiteMatInst);
	WhiteStickerR->SetMaterial(0,WhiteMatInst);
	WhiteStickerU->SetMaterial(0,WhiteMatInst);
	WhiteStickerD->SetMaterial(0,WhiteMatInst);
	WhiteStickerUL->SetMaterial(0,WhiteMatInst);
	WhiteStickerUR->SetMaterial(0,WhiteMatInst);
	WhiteStickerDL->SetMaterial(0,WhiteMatInst);
	WhiteStickerDR->SetMaterial(0,WhiteMatInst);
	// Use a spring arm to give the camera smooth, natural-feeling motion.
    USpringArmComponent* SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraAttachmentArm"));
    SpringArm->SetupAttachment(RootComponent);
    //SpringArm->SetRelativeRotation(FRotator(0.f, 180.f, 0.f));
    SpringArm->TargetArmLength = 1300.0f;
    SpringArm->bEnableCameraLag = true;
    SpringArm->CameraLagSpeed = 3.0f;
    // Create a camera and attach to our spring arm
    UCameraComponent* Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("ActualCamera"));
    Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	bInputTaken = true;
	Times = 0;
    // Take control of the default player
    AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void ASpeedCubePawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedCubePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ASpeedCubePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	//bind inputs to functions
	InputComponent->BindAction("LeftDown",IE_Pressed,this,&ASpeedCubePawn::MoveLeft);
	InputComponent->BindAction("MiddleDown",IE_Pressed,this,&ASpeedCubePawn::MoveMiddle);
	InputComponent->BindAction("MiddleUp",IE_Pressed,this,&ASpeedCubePawn::MoveMiddleUp);
	InputComponent->BindAction("LeftUp",IE_Pressed,this,&ASpeedCubePawn::MoveLeftUp);
	InputComponent->BindAction("RightUp",IE_Pressed,this,&ASpeedCubePawn::MoveRightUp);
	InputComponent->BindAction("RightDown",IE_Pressed,this,&ASpeedCubePawn::MoveRightDown);
	InputComponent->BindAction("TopLeft",IE_Pressed,this,&ASpeedCubePawn::MoveTopLeft);
	InputComponent->BindAction("TopRight",IE_Pressed,this,&ASpeedCubePawn::MoveTopRight);
	InputComponent->BindAction("BottomLeft",IE_Pressed,this,&ASpeedCubePawn::MoveBottomLeft);
	InputComponent->BindAction("BottomRight",IE_Pressed,this,&ASpeedCubePawn::MoveBottomRight);
	InputComponent->BindAction("MiddleLeft",IE_Pressed,this,&ASpeedCubePawn::MoveMiddleLeft);
	InputComponent->BindAction("MiddleRight",IE_Pressed,this,&ASpeedCubePawn::MoveMiddleRight);
	InputComponent->BindAction("FrontRight",IE_Pressed,this,&ASpeedCubePawn::MoveFrontRight);
	InputComponent->BindAction("FrontLeft",IE_Pressed,this,&ASpeedCubePawn::MoveFrontLeft);
	InputComponent->BindAction("BackRight",IE_Pressed,this,&ASpeedCubePawn::MoveBackRight);
	InputComponent->BindAction("BackLeft",IE_Pressed,this,&ASpeedCubePawn::MoveBackLeft);
	InputComponent->BindAction("MiddleFaceRight",IE_Pressed,this,&ASpeedCubePawn::MoveMiddleFaceRight);
	InputComponent->BindAction("MiddleFaceLeft",IE_Pressed,this,&ASpeedCubePawn::MoveMiddleFaceLeft);
	InputComponent->BindAction("PitchCubeUp",IE_Pressed,this,&ASpeedCubePawn::PitchCubeUp);
	InputComponent->BindAction("RollCubeUp",IE_Pressed,this,&ASpeedCubePawn::RollCubeUp);
	InputComponent->BindAction("YawCubeUp",IE_Pressed,this,&ASpeedCubePawn::YawCubeUp);
	InputComponent->BindAction("YawCubeDown",IE_Pressed,this,&ASpeedCubePawn::YawCubeDown);
	InputComponent->BindAction("PitchCubeDown",IE_Pressed,this,&ASpeedCubePawn::PitchCubeDown);
	InputComponent->BindAction("RollCubeDown",IE_Pressed,this,&ASpeedCubePawn::RollCubeDown);
	InputComponent->BindAction("Shuffle",IE_Pressed,this,&ASpeedCubePawn::ScrambleCube);
}
void ASpeedCubePawn::SetupMovement(uint8 Case)
{
	if(bInputTaken == true)
	{
		Times = 0;
		SideChange = Case;
		bInputTaken = false;
		GetWorldTimerManager().SetTimer(RepeatTimerHandle, this, &ASpeedCubePawn::CallMovement, 0.0000001f, false, 0.0f);
	}
}
void ASpeedCubePawn::MoveLeft()
{
	SetupMovement(2);
}
void ASpeedCubePawn::MoveLeftUp()
{
	SetupMovement(4);
}
void ASpeedCubePawn::MoveMiddle()
{
	if(bInputTaken == true)
	{
		SetupMovement(1);
	}
}
void ASpeedCubePawn::MoveRightUp()
{
	SetupMovement(6);
}
void ASpeedCubePawn::MoveRightDown()
{
	SetupMovement(5);
}
void ASpeedCubePawn::MoveTopLeft()
{
	SetupMovement(8);
}
void ASpeedCubePawn::MoveTopRight()
{
	SetupMovement(7);
}
void ASpeedCubePawn::MoveMiddleUp()
{
	SetupMovement(3);
}
void ASpeedCubePawn::MoveBottomLeft()
{
	SetupMovement(10);
}
void ASpeedCubePawn::MoveBottomRight()
{
	SetupMovement(9);
}
void ASpeedCubePawn::MoveMiddleLeft()
{
	SetupMovement(12);
}
void ASpeedCubePawn::MoveMiddleRight()
{
	SetupMovement(11);
}
void ASpeedCubePawn::MoveFrontRight()
{
	SetupMovement(13);
}
void ASpeedCubePawn::MoveFrontLeft()
{
	SetupMovement(14);
}
void ASpeedCubePawn::MoveBackRight()
{
	SetupMovement(15);
}
void ASpeedCubePawn::MoveBackLeft()
{
	SetupMovement(16);
}
void ASpeedCubePawn::MoveMiddleFaceRight()
{
	SetupMovement(17);
}
void ASpeedCubePawn::MoveMiddleFaceLeft()
{
	SetupMovement(18);
}
void ASpeedCubePawn::PitchCubeUp()
{
	SetupMovement(19);
}
void ASpeedCubePawn::RollCubeUp()
{
	SetupMovement(20);
}
void ASpeedCubePawn::PitchCubeDown()
{
	SetupMovement(23);
}
void ASpeedCubePawn::RollCubeDown()
{
	SetupMovement(24);
}
void ASpeedCubePawn::YawCubeUp()
{
	SetupMovement(21);
}
void ASpeedCubePawn::YawCubeDown()
{
	SetupMovement(22);
}
void ASpeedCubePawn::MovePart(int16 Funct1, int16 Funct2, int16 Target, int16 Dist)
{
	++Times;
	DoItAll(Funct1,Target,Dist);
	DoItAll(Funct2,Target,Dist);
	if(Times != 200)
	{
		GetWorldTimerManager().SetTimer(RepeatTimerHandle, this, &ASpeedCubePawn::CallMovement, 0.0000001f, false, 0.0f);
	}
	else
	{
		bInputTaken = true;
		SideChange = 0;
		DoItAll(16,0,0);
	}
}
void ASpeedCubePawn::CallMovement()
{
	switch(SideChange)
	{
		case 1:
			MovePart(2,4,0,200);
			break;
		case 2:
			MovePart(2,4,-200,283);
			break;
		case 3:
			MovePart(2,5,0,200);
			break;
		case 4:
			MovePart(2,5,-200,283);
			break;
		case 5:
			MovePart(2,4,200,283);
			break;
		case 6:
			MovePart(2,5,200,283);
			break;
		case 7:
			MovePart(3,6,200,283);
			break;
		case 8:
			MovePart(3,7,200,283);
			break;
		case 9:
			MovePart(3,6,-200,283);
			break;
		case 10:
			MovePart(3,7,-200,283);
			break;
		case 11:
			MovePart(3,6,0,200);
			break;
		case 12:
			MovePart(3,7,0,200);
			break;
		case 13:
			MovePart(1,8,-200,283);
			break;
		case 14:
			MovePart(1,9,-200,283);
			break;
		case 15:
			MovePart(1,8,200,283);
			break;
		case 16:
			MovePart(1,9,200,283);
			break;
		case 17:
			MovePart(1,8,0,200);
			break;
		case 18:
			MovePart(1,9,0,200);
			break;
		case 19:
			MovePart(2,10,0,0);
			break;
		case 20:
			MovePart(3,11,0,0);
			break;
		case 21:
			MovePart(1,12,0,0);
			break;
		case 22:
			MovePart(1,13,0,0);
			break;
		case 23:
			MovePart(3,14,0,0);
			break;
		case 24:
			MovePart(2,15,0,0);
			break;
	}
}
void ASpeedCubePawn::DoItAll(int16 Funct, int16 Arg,int16 Arg2)
{
	YellowMiddle->CallWhatever(Funct,Arg,Arg2,Times);
	RedMiddle->CallWhatever(Funct,Arg,Arg2,Times);
	GreenMiddle->CallWhatever(Funct,Arg,Arg2,Times);
	BlueMiddle->CallWhatever(Funct,Arg,Arg2,Times);
	OrangeMiddle->CallWhatever(Funct,Arg,Arg2,Times);
	WhiteMiddle->CallWhatever(Funct,Arg,Arg2,Times);
	RYEdge->CallWhatever(Funct,Arg,Arg2,Times);
	RWEdge->CallWhatever(Funct,Arg,Arg2,Times);
	BYEdge->CallWhatever(Funct,Arg,Arg2,Times);
	BWEdge->CallWhatever(Funct,Arg,Arg2,Times);
	GYEdge->CallWhatever(Funct,Arg,Arg2,Times);
	GWEdge->CallWhatever(Funct,Arg,Arg2,Times);
	OYEdge->CallWhatever(Funct,Arg,Arg2,Times);
	OWEdge->CallWhatever(Funct,Arg,Arg2,Times);
	RGEdge->CallWhatever(Funct,Arg,Arg2,Times);
	RBEdge->CallWhatever(Funct,Arg,Arg2,Times);
	OGEdge->CallWhatever(Funct,Arg,Arg2,Times);
	OBEdge->CallWhatever(Funct,Arg,Arg2,Times);
	RYBCorner->CallWhatever(Funct,Arg,Arg2,Times);
	RYGCorner->CallWhatever(Funct,Arg,Arg2,Times);
	RWBCorner->CallWhatever(Funct,Arg,Arg2,Times);
	RWGCorner->CallWhatever(Funct,Arg,Arg2,Times);
	OYBCorner->CallWhatever(Funct,Arg,Arg2,Times);
	OYGCorner->CallWhatever(Funct,Arg,Arg2,Times);
	OWBCorner->CallWhatever(Funct,Arg,Arg2,Times);
	OWGCorner->CallWhatever(Funct,Arg,Arg2,Times);
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("x: %f, y: %f, z; %f"), OYGCorner->GetX(), OYGCorner->GetY(),OYGCorner->GetZ()));
}
void ASpeedCubePawn::ScrambleCube()
{
	if(bInputTaken == true)
	{
		bInputTaken = false;
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Spacebar pressed"));
		DoItAll(17,0,0);
		srand(GetGameTimeSinceCreation());
		for(uint8 i = 0; i < 12; i++)
		{
			int32 RandomNumber = rand() % 12 + 1;
			Edges[0] = RYEdge->GetCase();
			Edges[1] = RWEdge->GetCase();
			Edges[2] = RGEdge->GetCase();
			Edges[3] = RBEdge->GetCase();
			Edges[4] = OYEdge->GetCase();
			Edges[5] = OWEdge->GetCase();
			Edges[6] = OGEdge->GetCase();
			Edges[7] = OBEdge->GetCase();
			Edges[8] = BWEdge->GetCase();
			Edges[9] = GWEdge->GetCase();
			Edges[10] = BYEdge->GetCase();
			Edges[11] = GYEdge->GetCase();
			//bool Corners[8];
			//bool Middles[4];
			if(CompareArray(RandomNumber))
			{
				Edges[CheckIsScrambled()] = RandomNumber;
			}
			CorrectArray();
		}
	}
	DoItAll(18,0,0);
	bInputTaken = true;
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Done"));
}
bool ASpeedCubePawn::CompareArray(int32 Target)
{
	for(uint8 i = 0; i < 12; i++)
	{
		if(Edges[i] == Target)
		{
			return false;
		}
	}
	return true;
}
uint8 ASpeedCubePawn::CheckIsScrambled()
{
	for(uint8 i = 0; i < 12; i++)
	{
		if(Edges[i] == 0)
		{
			return i;
		}
	}
	return 12;
}
void ASpeedCubePawn::CorrectArray()
{
	RYEdge->SetCase(Edges[0]);
	RWEdge->SetCase(Edges[1]);
	RGEdge->SetCase(Edges[2]);
	RBEdge->SetCase(Edges[3]);
	OYEdge->SetCase(Edges[4]);
	OWEdge->SetCase(Edges[5]);
	OGEdge->SetCase(Edges[6]);
	OBEdge->SetCase(Edges[7]);
	BWEdge->SetCase(Edges[8]);
	GWEdge->SetCase(Edges[9]);
	BYEdge->SetCase(Edges[10]);
	GYEdge->SetCase(Edges[11]);
}