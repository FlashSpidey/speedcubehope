// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedCube.h"

// Sets default values
ASpeedCube::ASpeedCube()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASpeedCube::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedCube::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

