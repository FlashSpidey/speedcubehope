// Copyright Epic Games, Inc. All Rights Reserved.

#include "SpeedCubeHope.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SpeedCubeHope, "SpeedCubeHope" );
