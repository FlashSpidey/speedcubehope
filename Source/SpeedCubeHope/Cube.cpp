// Fill out your copyright notice in the Description page of Project Settings.


#include "Cube.h"

UCube::UCube()
{
    DistanceFromSpeedCube = 0;
	Position = FVector(0.0f);
	Rotation = FRotator(0.0f);
}
UCube::~UCube(){};
void UCube::FullSet(FVector Start, float Dist)
{
	SetWorldScale3D(FVector(2.0f));
	SetPosition(Start);
	SetRelativeLocation(GetPosition());
    SetDistanceFromSpeedCube(Dist);
}
void UCube::FullSet2(FVector Start, FRotator StartRotation, float Dist)
{
    SetWorldScale3D(FVector(1.0f));
    SetRotation(StartRotation);
	SetPosition(Start);
    SetRelativeLocationAndRotation(Start,StartRotation);
    SetDistanceFromSpeedCube(Dist);
}
void UCube::FullSet3(FVector Start, FVector Scale, float Dist)
{
	SetWorldScale3D(Scale);
	SetPosition(Start);
	SetRelativeLocation(GetPosition());
    SetDistanceFromSpeedCube(Dist);
}
FVector UCube::GetPosition()
{
    return Position;
}
FRotator UCube::GetRotation()
{
    return Rotation;
}
float UCube::GetX()
{
    return Position.X;
}
float UCube::GetY()
{
    return Position.Y;
}
float UCube::GetZ()
{
    return Position.Z;
}
float UCube::GetRoll()
{
    return Rotation.Roll;
}
float UCube::GetYaw()
{
    return Rotation.Yaw;
}
float UCube::GetPitch()
{
    return Rotation.Pitch;
}
bool UCube::GetShouldMove()
{
    return bShouldMove;
}
float UCube::GetDist()
{
    return DistanceFromSpeedCube;
}
int32 UCube::GetCase()
{
    return Case;
}
void UCube::SetPosition(FVector NewPos)
{
    Position = NewPos;
}
void UCube::SetRotation(FRotator NewRot)
{
    Rotation = NewRot;
}
void UCube::SetDistanceFromSpeedCube(float Dist)
{
    DistanceFromSpeedCube = Dist;
}
void UCube::SetX(float Number)
{
    Position.X = Number;
}
void UCube::SetY(float Number)
{
    Position.Y = Number;
}
void UCube::SetZ(float Number)
{
    Position.Z = Number;
}
void UCube::SetPitch(float Number)
{
    Rotation.Pitch = Number;
}
void UCube::SetRoll(float Number)
{
    Rotation.Roll = Number;
}
void UCube::SetYaw(float Number)
{
    Rotation.Yaw = Number;
}
void UCube::SetShouldMove(bool Answer)
{
    bShouldMove = Answer;
}
void UCube::SetCase(int32 NewCase)
{
    Case = NewCase;
}
void UCube::ChangeX(float Change)
{
    Position.X += Change;
}
void UCube::ChangeY(float Change)
{
    Position.Y += Change;
}
void UCube::ChangeZ(float Change)
{
    Position.Z += Change;
}
void UCube::ChangePitch(float Change)
{
    Rotation.Pitch += Change;
}
void UCube::ChangeRoll(float Change)
{
    Rotation.Roll += Change;
}
void UCube::ChangeYaw(float Change)
{
    Rotation.Yaw += Change;
}
float UCube::PythagoreanTheorem(float Number)
{
    //does pytharogean theorem to solve for b given c and a
    float C = FMath::Square(DistanceFromSpeedCube);
    float A = FMath::Square(Number);
    float B = C-A;
    return sqrt(B);
}
float UCube::ThirdPythagorean(float Number, float OtherNumber)
{
    float C = FMath::Square(DistanceFromSpeedCube);
    float X = FMath::Square(Number);
    float Y = FMath::Square(OtherNumber);
    float Z = C-(X+Y);
    if(Z<0)
    {
        Z=0;
        //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Uh oh"));
    }
    return sqrt(Z);
}
void UCube::MoveDown(int16 Times)
{
    if(bShouldMove==true)
    {
        if(GetX() < 0 || (GetX() == 0 && GetZ() > 0))
        {
            ChangeZ(-1);
            SetX((ThirdPythagorean(GetZ(),GetY()))*-1);
        }
        else
        {
            ChangeZ(1);
            SetX(ThirdPythagorean(GetZ(),GetY()));
        }
        RotatePiece(1);
        SetRelativeLocation(GetPosition());
    }
}
void UCube::MoveUp(int16 Times)
{
    if(bShouldMove == true)
    {
        if(GetX() < 0 || (GetX() == 0 && GetZ() < 0))
        {
            ChangeZ(1);
            SetX((ThirdPythagorean(GetZ(),GetY()))*-1);
        }
        else
        {
            ChangeZ(-1);
            SetX(ThirdPythagorean(GetZ(),GetY()));
        }
        RotatePiece(2);
        SetRelativeLocation(GetPosition());
    }
}
void UCube::MoveEdgeDown(int16 Times)
{
    if(bShouldMove == true)
    {
        if(GetZ() >= 200 || GetZ() <= -200)
        {
            if(Times < 166)
            {
                if(GetX() < 0 || (GetX() == 0 && GetZ() > 0))
                {
                    ChangeZ(-1);
                    SetX((ThirdPythagorean(GetZ(),GetY()))*-1);
                }
                else
                {
                    ChangeZ(1);
                    SetX(ThirdPythagorean(GetZ(),GetY()));
                }
            }
        }
        else if(GetX() < 0 || (GetX() == 0 && GetZ() > 0))
        {
            ChangeZ(-2);
            SetX((ThirdPythagorean(GetZ(),GetY()))*-1);
        }
        else
        {
            ChangeZ(2);
            SetX(ThirdPythagorean(GetZ(),GetY()));
        }
        RotatePiece(1);
        SetRelativeLocation(GetPosition());
    }
}
void UCube::MoveEdgeUp(int16 Times)
{
    if(bShouldMove == true)
    {
        if(GetZ() >= 200 || GetZ() <= -200)
        {
            if(Times < 166)
            {
                if(GetX() < 0 || (GetX() == 0 && GetZ() < 0))
                {
                    ChangeZ(1);
                    SetX((ThirdPythagorean(GetZ(),GetY()))*-1);
                }
                else
                {
                    ChangeZ(-1);
                    SetX(ThirdPythagorean(GetZ(),GetY()));
                }
            }
        }
        else if(GetX() < 0 || (GetX() == 0 && GetZ() < 0))
        {
            ChangeZ(2);
            SetX((ThirdPythagorean(GetZ(),GetY()))*-1);
        }
        else
        {
            ChangeZ(-2);
            SetX(ThirdPythagorean(GetZ(),GetY()));
        }
        RotatePiece(2);
        SetRelativeLocation(GetPosition());
    }
}
void UCube::MoveLeft()
{
    if(bShouldMove==true)
    {
        if(GetX() < 0 || (GetX() == 0 && GetY() > 0))
        {
            ChangeY(-1);
            SetX((ThirdPythagorean(GetZ(),GetY()))*-1);
        }
        else
        {
            ChangeY(1);
            SetX(ThirdPythagorean(GetZ(),GetY()));
        }
        RotatePiece(3);
        SetRelativeLocation(GetPosition());
    }
}
void UCube::MoveEdgeLeft(int16 Times)
{
    if(bShouldMove == true)
    {
        if(GetY() >= 200 || GetY() <= -200)
        {
            if(Times < 166)
            {
                if(GetX() < 0 || (GetX() == 0 && GetY() > 0))
                {
                    ChangeY(-1);
                    SetX((ThirdPythagorean(GetZ(),GetY()))*-1);
                }
                else
                {
                    ChangeY(1);
                    SetX(ThirdPythagorean(GetZ(),GetY()));
                }
            }
        }
        else if(GetX() < 0 || (GetX() == 0 && GetY() > 0))
        {
            ChangeY(-2);
            SetX((ThirdPythagorean(GetZ(),GetY()))*-1);
        }
        else
        {
            ChangeY(2);
            SetX(ThirdPythagorean(GetZ(),GetY()));
        }
        RotatePiece(3);
        SetRelativeLocation(GetPosition());
    }
}
void UCube::MoveRight()
{
    if(bShouldMove == true)
    {
        if(GetX() < 0 || (GetX() == 0 && GetY() < 0))
        {
            ChangeY(1);
            SetX((ThirdPythagorean(GetZ(),GetY()))*-1);
        }
        else
        {
            ChangeY(-1);
            SetX(ThirdPythagorean(GetZ(),GetY()));
        }
        RotatePiece(4);
        SetRelativeLocation(GetPosition());
    }
}
void UCube::MoveEdgeRight(int16 Times)
{
    if(bShouldMove == true)
    {
        if(GetY() >= 200 || GetY() <= -200)
        {
            if(Times < 166)
            {
                if(GetX() < 0 || (GetX() == 0 && GetY() < 0))
                {
                    ChangeY(1);
                    SetX((ThirdPythagorean(GetZ(),GetY()))*-1);
                }
                else
                {
                    ChangeY(-1);
                    SetX(ThirdPythagorean(GetZ(),GetY()));
                }
            }
        }
        else if(GetX() < 0 || (GetX() == 0 && GetY() < 0))
        {
            ChangeY(2);
            SetX((ThirdPythagorean(GetZ(),GetY()))*-1);
        }
        else
        {
            ChangeY(-2);
            SetX(ThirdPythagorean(GetZ(),GetY()));
        }
        RotatePiece(4);
        SetRelativeLocation(GetPosition());
    }
}
void UCube::MoveFaceLeft(int16 Times)
{
    if(bShouldMove == true)
    {
        if(GetZ() < 0 || (GetZ() == 0 && GetY() < 0))
        {
            ChangeY(1);
            SetZ((ThirdPythagorean(GetX(),GetY()))*-1);
        }
        else
        {
            ChangeY(-1);
            SetZ(ThirdPythagorean(GetX(),GetY()));
        }
        RotatePiece(6);
        SetRelativeLocation(GetPosition());
    }
}
void UCube::MoveFaceEdgeLeft(int16 Times)
{
    if(bShouldMove == true)
    {
        if(GetY() >= 200 || GetY() <= -200)
        {
            if(Times < 166)
            {
                if(GetZ() < 0 || (GetZ() == 0 && GetY() < 0))
                {
                    ChangeY(1);
                    SetZ((ThirdPythagorean(GetX(),GetY()))*-1);
                }
                else
                {
                    ChangeY(-1);
                    SetZ(ThirdPythagorean(GetX(),GetY()));
                }
            }
        }
        else if(GetZ() < 0 || (GetZ() == 0 && GetY() < 0))
        {
            ChangeY(2);
            SetZ((ThirdPythagorean(GetX(),GetY()))*-1);
        }
        else
        {
            ChangeY(-2);
            SetZ(ThirdPythagorean(GetX(),GetY()));
        }
        RotatePiece(6);
        SetRelativeLocation(GetPosition());
    }
}
void UCube::MoveFaceRight(int16 Times)
{
    if(bShouldMove==true)
    {
        if(GetZ() < 0 || (GetZ() == 0 && GetY() > 0))
        {
            ChangeY(-1);
            SetZ((ThirdPythagorean(GetX(),GetY()))*-1);
        }
        else
        {
            ChangeY(1);
            SetZ(ThirdPythagorean(GetX(),GetY()));
        }
        RotatePiece(5);
        SetRelativeLocation(GetPosition());
    }
}
void UCube::MoveFaceEdgeRight(int16 Times)
{
    if(bShouldMove == true)
    {
        if(GetY() >= 200 || GetY() <= -200)
        {
            if(Times < 166)
            {
                if(GetZ() < 0 || (GetZ() == 0 && GetY() > 0))
                {
                    ChangeY(-1);
                    SetZ((ThirdPythagorean(GetX(),GetY()))*-1);
                }
                else
                {
                    ChangeY(1);
                    SetZ(ThirdPythagorean(GetX(),GetY()));
                }
            }
        }
        else if(GetZ() < 0 || (GetZ() == 0 && GetY() > 0))
        {
            ChangeY(-2);
            SetZ((ThirdPythagorean(GetX(),GetY()))*-1);
        }
        else
        {
            ChangeY(2);
            SetZ(ThirdPythagorean(GetX(),GetY()));
        }
        RotatePiece(5);
        SetRelativeLocation(GetPosition());
    }
}
void UCube::CheckX(int16 Target)
{
    if(GetX() == Target)
    {
        bShouldMove = true;
    }
    else
    {
        bShouldMove = false;
    }
}
void UCube::CheckY(int16 Target)
{
    if(GetY() == Target)
    {
        bShouldMove = true;
    }
    else
    {
        bShouldMove = false;
    }
}void UCube::CheckZ(int16 Target)
{
    if(GetZ() == Target)
    {
        bShouldMove = true;
    }
    else
    {
        bShouldMove = false;
    }
}
void UCube::CallWhatever(int16 Number,int16 Arg, int16 Arg2, int16 Arg3)
{
    switch(Number)
    {
        case 1:
            CheckX(Arg);
            break;
        case 2:
            CheckY(Arg);
            break;
        case 3:
            CheckZ(Arg);
            break;
        case 4:
            if(DistanceFromSpeedCube == Arg2)
            {
                MoveDown(Arg3);
            }
            else
            {
                MoveEdgeDown(Arg3);
            }
            break;
        case 5:
            if(DistanceFromSpeedCube == Arg2)
            {
                MoveUp(Arg3);
            }
            else
            {
                MoveEdgeUp(Arg3);
            }
            break;
        case 6:
            if(DistanceFromSpeedCube == Arg2)
            {
                MoveRight();
            }
            else
            {
                MoveEdgeRight(Arg3);
            }
            break;
        case 7:
            if(DistanceFromSpeedCube == Arg2)
            {
                MoveLeft();
            }
            else
            {
                MoveEdgeLeft(Arg3);
            }
            break;
        case 8:
            if(DistanceFromSpeedCube == Arg2)
            {
                MoveFaceRight(Arg3);
            }
            else
            {
                MoveFaceEdgeRight(Arg3);
            }
            break;
        case 9:
            if(DistanceFromSpeedCube == Arg2)
            {
                MoveFaceLeft(Arg3);
            }
            else
            {
                MoveFaceEdgeLeft(Arg3);
            }
            break;
        case 10:
            SetShouldMove(true);
            if(DistanceFromSpeedCube == 200 || (GetDist() == 283 && GetY() != 0))
            {
                MoveDown(Arg3);
            }
            else
            {
                MoveEdgeDown(Arg3);
            }
            break;
        case 11:
            SetShouldMove(true);
            if(DistanceFromSpeedCube == 200 || (GetDist() == 283 && GetZ() != 0))
            {
                MoveRight();
            }
            else
            {
                MoveEdgeRight(Arg3);
            }
            break;
        case 12:
            SetShouldMove(true);
            if(DistanceFromSpeedCube == 200 || (GetDist() == 283 && GetY() != 0))
            {
                MoveFaceRight(Arg3);
            }
            else
            {
                MoveFaceEdgeRight(Arg3);
            }
            break;
        case 13:
            SetShouldMove(true);
            if(DistanceFromSpeedCube == 200 || (GetDist() == 283 && GetY() != 0))
            {
                MoveFaceLeft(Arg3);
            }
            else
            {
                MoveFaceEdgeLeft(Arg3);
            }
            break;
        case 14:
            SetShouldMove(true);
            if(DistanceFromSpeedCube == 200 || (GetDist() == 283 && GetY() != 0))
            {
                MoveUp(Arg3);
            }
            else
            {
                MoveEdgeUp(Arg3);
            }
            break;
         case 15:
            SetShouldMove(true);
            if(DistanceFromSpeedCube == 200 || (GetDist() == 283 && GetZ() != 0))
            {
                MoveLeft();
            }
            else
            {
                MoveEdgeLeft(Arg3);
            }
            break;
        case 16:
            CorrectPositions();
            break;
        case 17:
            SetCase(Arg);
            break;
        case 18:
            Scramble();
            break;
    }
}
void UCube::CorrectPositions()
{
    if(200-GetX() > 300)
    {
        SetX(-200);
    }
    else if(200-GetX() >100)
    {
        SetX(0);
    }
    else
    {
        SetX(200);
    }
    if(200-GetY() > 300)
    {
        SetY(-200);
    }
    else if(200-GetY() >100)
    {
        SetY(0);
    }
    else
    {
        SetY(200);
    }
    if(200-GetZ() > 300)
    {
        SetZ(-200);
    }
    else if(200-GetZ() >100)
    {
        SetZ(0);
    }
    else
    {
        SetZ(200);
    }
    SetRotation(GetRelativeRotation());
    if(90-GetPitch() < -135)
    {
        SetPitch(-90);
    }
    else if(90-GetPitch() < -45)
    {
        SetPitch(180);
    }
    else if(90-GetPitch() < 45)
    {
        SetPitch(90);
    }
    else if(90-GetPitch() < 135)
    {
        SetPitch(0);
    }
    else if(90-GetPitch() < 225)
    {
        SetPitch(-90);
    }
    else
    {
        SetPitch(180);
    }
    if(90-GetRoll() < -135)
    {
        SetRoll(-90);
    }
    else if(90-GetRoll() < -45)
    {
        SetRoll(180);
    }
    else if(90-GetRoll() < 45)
    {
        SetRoll(90);
    }
    else if(90-GetRoll() < 135)
    {
        SetRoll(0);
    }
    else if(90-GetRoll() < 225)
    {
        SetRoll(-90);
    }
    else
    {
        SetRoll(180);
    }
    if(90-GetYaw() < -135)
    {
        SetYaw(-90);
    }
    else if(90-GetYaw() < -45)
    {
        SetYaw(180);
    }
    else if(90-GetYaw() < 45)
    {
        SetYaw(90);
    }
    else if(90-GetYaw() < 135)
    {
        SetYaw(0);
    }
    else if(90-GetYaw() < 225)
    {
        SetYaw(-90);
    }
    else
    {
        SetYaw(180);
    }
}
void UCube::RotatePiece(int16 RotationMode)
{
    switch(RotationMode)
    {
        case 1:
            AddWorldRotation(FRotator(0.45,0.0,0.0));
            break;
        case 2:
            AddWorldRotation(FRotator(-0.45,0.0,0.0));
            break;
        case 3:
            AddWorldRotation(FRotator(0.0,0.45,0.0));
            break;
        case 4:
            AddWorldRotation(FRotator(0.0,-0.45,0.0));
            break;
        case 5:
            AddWorldRotation(FRotator(0.0,0.0,0.45));
            break;
        case 6:
            AddWorldRotation(FRotator(0.0,0.0,-0.45));
            break;
    }
}
void UCube::Scramble()
{
    switch(Case)
    {
        case 1:
            SetPosition(FVector(200.0f,200.0f,0.0f));
            break;
        case 2:
            SetPosition(FVector(200.0f,0.0f,200.0f));
            break;
        case 3:
            SetPosition(FVector(0.0f,200.0f,200.0f));
            break;
        case 4:
            SetPosition(FVector(-200.0f,-200.0f,0.0f));
            break;
        case 5:
            SetPosition(FVector(-200.0f,0.0f,-200.0f));
            break;
        case 6:
            SetPosition(FVector(0.0f,-200.0f,-200.0f));
            break;
        case 7:
            SetPosition(FVector(-200.0f,200.0f,0.0f));
            break;
        case 8:
            SetPosition(FVector(200.0f,-200.0f,0.0f));
            break;
        case 9:
            SetPosition(FVector(200.0f,0.0f,-20.0f));
            break;
        case 10:
            SetPosition(FVector(-200.0f,0.0f,200.0f));
            break;
        case 11:
            SetPosition(FVector(0.0f,-200.0f,200.0f));
            break;
        case 12:
            SetPosition(FVector(0.0f,200.0f,-200.0f));
            break;
    }
    SetRelativeLocation(GetPosition());
}