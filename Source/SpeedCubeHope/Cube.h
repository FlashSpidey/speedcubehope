// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Math/UnrealMathUtility.h"
#include "Cube.generated.h"

/**
 * 
 */
UCLASS()
class SPEEDCUBEHOPE_API UCube : public UStaticMeshComponent
{
	GENERATED_BODY()
	public:
	UCube();
	~UCube();
	void FullSet(FVector, float);
	void FullSet2(FVector,FRotator,float);
	void FullSet3(FVector,FVector,float);
	//getters
	FVector GetPosition();
	FRotator GetRotation();
	float GetX();
	float GetY();
	float GetZ();
	float GetPitch();
	float GetRoll();
	float GetYaw();
	bool GetShouldMove();
	float GetDist();
	int32 GetCase();
	//setters
	void SetPosition(FVector);
	void SetRotation(FRotator);
	void SetDistanceFromSpeedCube(float);
	void SetX(float);
	void SetY(float);
	void SetZ(float);
	void SetPitch(float);
	void SetRoll(float);
	void SetYaw(float);
	void SetShouldMove(bool);
	void SetCase(int32);
	//changers?
	void ChangeX(float);
	void ChangeY(float);
	void ChangeZ(float);
	void ChangePitch(float);
	void ChangeRoll(float);
	void ChangeYaw(float);
	//other(mostly movement)
	float PythagoreanTheorem(float);
	float ThirdPythagorean(float,float);
	void MoveDown(int16);
	void MoveUp(int16);
	void MoveEdgeDown(int16);
	void MoveEdgeUp(int16);
	void MoveLeft();
	void MoveEdgeLeft(int16);
	void MoveRight();
	void MoveEdgeRight(int16);
	void MoveFaceRight(int16);
	void MoveFaceEdgeRight(int16);
	void MoveFaceLeft(int16);
	void MoveFaceEdgeLeft(int16);
	void CallWhatever(int16,int16,int16,int16);
	void CheckX(int16);
	void CheckY(int16);
	void CheckZ(int16);
	void CorrectPositions();
	void RotatePiece(int16);
	void Scramble();
private:
	float DistanceFromSpeedCube;
	FVector Position;
	FRotator Rotation;
	bool bShouldMove;
	float YawValue;
	float PitchValue;
	float RollValue;
	int32 Case;
};
